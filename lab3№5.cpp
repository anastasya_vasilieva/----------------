#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define SIZE 1024
#define LEN 20    
 
char **ArrayOfWords(char *s, int &num, int &rows)
{   char **arr, *word;
    int i, N;

    N = strlen(s) / 2;
    arr = (char**) malloc(N);
    for (i = 0; i<N; i++)
        arr[i] = (char*) malloc(LEN);
 
    word = strtok (s," ,.-");
    strcpy(arr[0], word);
    i = 1;
 
    while (word != NULL)
    {   word = strtok (NULL, " ,.-");
        if (word == NULL) break;
        strcpy(arr[i], word);
        i++;
    }
    rows = N;   
    num = i;    
    return arr;
}
 
void Swap (char *a, char *b)
{   char *temp = (char*) malloc (LEN);
    strcpy(temp, a);
    strcpy(a, b);
    strcpy(b, temp);
}
 
void Sorted (char **arr, int num)
{   int i, j;
    for (i = 0; i<num-1; i++)
        for (j = i+1; j<num; j++)
            if (strlen(arr[i]) > strlen(arr[j]))
                Swap (arr[i], arr[j]);
}
 
int main()
{   char buf[SIZE], **arr;
    int i, M = 0, rows = 0;
    
    printf("Enter text: ");
    fgets(buf, SIZE, stdin);
    buf[strlen(buf) - 1] = '\0';
 
    arr = ArrayOfWords(buf, M, rows);
    Sorted (arr, M);
    putchar ('\n');
    printf("Your line in ascending order: \n");
    for (i = 0; i<M; i++)
        puts(arr[i]);
 
    getchar();
    return 0;
}