
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 10
#define M 10
int main ()
{
	int matr [N][M];
	int i,j,pos=0,neg=0;
	srand(time(0));
	for (i=0; i<N; i++)
		for (j=0; j<M; j++)
			matr [i][j]=rand()%100-50;
	for (i=0; i<N; i++)
	{
		for (j=0; j<M; j++)
			printf (" %3d", matr[i][j]);
		putchar ('\n');
	}
	for (i=0; i<N; i++)
		for (j=0; j<M; j++)
			if (matr [i][j]>=0)
				pos++;
			else
				neg++;
	printf ("Positive=%d, Negative=%d\n", pos, neg);
	if (pos>neg)
		printf ("Positive is more");
	else
		printf ("Negative is more");
	putchar ('\n');
	return 0;
}