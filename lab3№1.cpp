#include <stdio.h>
#include <string.h>
#include <math.h>
int main ()
{
	int i, first, count=0, inNum=0,  max=5, summa=0, digit=0, c=0;
	char str[256];
	printf ("Entering number must'not be more than 5 digits: ");
	putchar('\n');
	fgets(str, 256, stdin);
	str[strlen(str)-1]=' ';
	while (str[i])
	{
		if (str[i]!=' ' && inNum==0 && str[i]>='0' && str[i]<='9')
		{
			inNum=1;
			c=1;
			first=i;
		}
		else if (str[i]>='0' && str[i]<='9' && inNum==1 && max>c)
			c++;
		else if ((str[i]<'0' || str[i]>'9') && inNum==1)
		{
			inNum=0;
			for (int a=first; a<c+first; a++)
			{
				digit=digit*10+(str[a]-'0');
			}
			summa+=digit;
			digit=0;
			c=0;
		}
		i++;
	}
	printf ("Amount of your numbers is: %d", summa);
	putchar ('\n');
	return 0;
}